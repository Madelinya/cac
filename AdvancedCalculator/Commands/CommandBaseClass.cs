﻿using System;
using System.Collections.Generic;


namespace AdvancedCalculator
{
    /// <summary>
    /// Prefab for all command classes
    /// </summary>
    abstract class Command
    {
        public string Alias;
        public Command(string ali)
        {
            Alias = ali;
        }
    }
}