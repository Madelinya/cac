﻿using System;
using System.Collections.Generic;


namespace AdvancedCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            Commands commands = new Commands();
            GetCommandInput();
            Console.ReadKey();
        }
        private static void GetCommandInput()    // gets input as string, splits it and passes it to EvalCommand
        {
            var commandInput = Console.ReadLine();
            if (commandInput == "")    //checks for emtpty string
            {
                commandInput = "noCmdException";
            }
            var commandInputChecked = new List<string>(commandInput.Split(" "));
            commandInputChecked.RemoveAll(a => a.Equals(""));
            Commands.EvalCommandInput(commandInputChecked);    //passes command for evaluation
        }

    }
    
    /// <summary>
    /// This class deals with all command related behaviour and other stuff, in its constructor different command classes
    /// are created and then passed into an array of objects, was done this way to make expanding easier and hassle free.
    /// </summary>
    class Commands 
    {
        public Commands()
        {
            MatrixOperations matricies = new MatrixOperations("matrix");    //matrix class
            
            
            Command[] commands = new Command[]    //array declaration where to add the objects
            {
                matricies,
                
            };
        }
        public static void EvalCommandInput(List<string> commandInput) //evaluates command and routes it where it needs to go
        {
            foreach (var command in commandInput)
            {
                Console.WriteLine(command);
            }
        }
    }
}